# thinking to use the same pyrogram's config.ini
# from configparser import ConfigParser
import fdb

from CONFIG import CHAR, DB, HOST, PASS, PORT, USER


def get_recepcio(recnum, cursor):
    cursor.execute('SELECT * FROM RECEPCIO WHERE RECNUM={0}'.format(recnum))
    try:
        return cursor.fetchall()[0]
    except IndexError:
        # THIS IS WRONG!! BECOUSE I KNOW THAT partebot.py
        # WILL TAKE ValueError AND SEND A MESSAGE
        # TELLING TO THE USER THAT THE ID(RECNUM) IS WRONG, NOT ISOLATED
        raise ValueError


def get_clientes(codigo, cursor):
    cursor.execute('SELECT * FROM CLIENTES WHERE CODIGO={0}'.format(codigo))
    return cursor.fetchall()[0]


def parse(recep, cliente):
    # date's components
    # ID, CLIENT NAME, ADDRESS, ADDRESS INFO, CITY,
    # PHONE, PRODUCT, BRAND, INFO, INFO
    parte = [str(recep[0]), '**'+recep[4]+'**', cliente[3], cliente[4], cliente[5],
             '**' + cliente[7] + '**', recep[5], recep[6], recep[10], recep[11]]
    return [' '.join(parte), '+'.join([cliente[3], cliente[5]]).replace(' ', '+')]


def main(ID):
    # A bit of checking
    if HOST:
        DSN = '{0}/{1}:{2}'.format(HOST, PORT, DB)
    else:
        return 'Empty config file'

    con = fdb.connect(dsn=DSN, user=USER, password=PASS, charset=CHAR)

    recep = get_recepcio(ID, con.cursor())
    cliente = get_clientes(recep[3], con.cursor())  # client ID

    con.rollback()
    con.close()

    return parse(recep, cliente)


if __name__ == '__main__':
    main()
