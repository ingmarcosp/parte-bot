#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Documentation bellow.
'''
###############################################################################
#                                   ParteBot
#
# It's a personalized Bot to send date's information to tecnician's trough
# telegram directly from the company telegram account, interface is in spanish
#
# Thankst to:
# - Pyrogram
#
# wihtout tests... like a machou
###############################################################################

import sys
#import pdb
from pathlib import Path
from time import sleep
from pyrogram import Client, filters, idle
from pyrogram.handlers import MessageHandler
from pyrogram.types import TermsOfService

import fdb_driver
from CONFIG import TECHINICIAN, TEST_MODE, TG_CODE, TG_PHONE, TG_USER

PARTEBOT = Client('partebot', test_mode=TEST_MODE)
SESSIONS_FILE = Path('partebot.session')
CONFIG_FILE = Path('CONFIG.py')


@PARTEBOT.on_message(filters.chat('me') & filters.command(['mapa']))
def get_geo_data(client, message):
    IDs = message['text'].split(' ')
    IDs.pop(0)
    for ID in IDs:
        try:
            parte, address = fdb_driver.main(int(ID))
            client.send_message('me', ID + ':\n https://www.google.com/maps/search/?api=1&query=' + address)
        except ValueError:
            client.send_message('me',
                                'El ID: ' + ID + ' es incorrecto vuelva a enviarlo.')
        finally:
            pass


# TODO use decorator to add the handler with filters
@PARTEBOT.on_message(filters.text & filters.chat('me'))
def send(client, message):
    ''' Detects the command "ENVIAR", detects the date's ID and send to the technicians.'''
    # pdb.set_trace()
    print(message['text'])
    text = message['text']
    if text and 'ENVIAR' in text:  # send command
        text = text.split(' ')
        text.pop(0)  # delete ENVIAR command
        techni = text.pop(0)  # delete techician name
        if techni in TECHINICIAN.keys():
            tguser = TECHINICIAN[techni]
            client.send_message('me', 'Enviando partes a: ' + techni)
            for ID in text:
                try:
                    parte, address = fdb_driver.main(int(ID))
                    client.send_message(tguser, parte +
                                        '\n\n https://www.google.com/maps/search/?api=1&query=' + address)
                except ValueError:
                    client.send_message('me',
                                        'El ID: ' + ID + ' es incorrecto vuelva a enviarlo.')
                finally:
                    pass
        else:
            client.send_message('me',
                                'El técnico: ' + techni + ', no existe.')


def clear_code():
    ''' clearing old code '''
    config_list = CONFIG_FILE.read_text().split('\n')
    for conf in config_list:
        if 'TG_CODE' in conf:
            i = config_list.index(conf)
            config_list[i] = "TG_CODE = ''"
    CONFIG_FILE.write_text('\n'.join(config_list))


def get_code(TG_CODE=TG_CODE):
    ''' getting code '''
    while True:
        print('Code: ' + TG_CODE)
        if not TG_CODE:
            print('Empty security code')
            del(sys.modules['CONFIG'])
            del(TG_CODE)
            print('waiting 30 seconds until code arrive...')
            sleep(30)
            from CONFIG import TG_CODE
        else:
            break
    return TG_CODE


def login():
    ''' This is needed to build the Docker container, and make
    it the most automatic posible '''
    try:
        PARTEBOT.connect()
        print('Sending scurity code')
        sent_code = PARTEBOT.send_code(TG_PHONE)

        signed_in = PARTEBOT.sign_in(TG_PHONE,
                                     sent_code.phone_code_hash,
                                     get_code())

        if isinstance(signed_in, TermsOfService):
            print('Accepting')
            PARTEBOT.accept_terms_of_service(signed_in.id)
        elif not signed_in:
            print('Phone number not registered')
            sys.exit()
        print('Login complete!')

    except Exception as err:
        print(err)
        SESSIONS_FILE.unlink()
        clear_code()
        print('Clearing old code.')
    finally:
        print('Disconnecting')
        PARTEBOT.disconnect()


def main():
    ''' Main '''
    if SESSIONS_FILE.exists():
        print("Starting ParteBot")
        with PARTEBOT:
            idle()
    else:
        print('Login in to Telegram')
        login()
        main()


if __name__ == '__main__':
#    pdb.set_trace()
    main()
