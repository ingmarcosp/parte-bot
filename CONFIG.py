#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Pyrogram config
api_id =
api_hash =

# FIREBIRD CONFIG
HOST = '' # name o ip
PORT = 3050 # change to specific port
DB = '' # location in server
USER = ''
PASS = ''
CHAR = ''

# TELEGRAM CONFIG
TG_USER = ''
TG_PHONE = ''
TG_CODE = ''
TEST_MODE = False
TECHINICIAN = {} # KEY: sting NAME (in uppercase) - Value: string telegram user name without '@'
