VIRTUAL_ENV=$(HOME)/Venvs/telegram/bin/activate

DEV_INI=./config.ini.dev
OLD_INI=./config.ini.old

DEV_PY=./CONFIG.py.dev
OLD_PY=./CONFIG.py.old

SESSION=./partebot.session
CONTENT_PY=\#!/usr/bin/env python\n\# -*- coding: utf-8 -*- \#\n\n\# FIREBIRD CONFIG\nHOST = '' \# name o ip\nPORT = 3050 \# change to specific port\nDB = '' \# location in server\nUSER = ''\nPASS = ''\nCHAR = ''\n\n\# TELEGRAM CONFIG\nTG_USER = ''\nTG_PHONE = ''\nTG_CODE = ''\nTEST_MODE = False\nTECHINICIAN = {} \# KEY: sting NAME (in uppercase) - Value: string telegram user name without '@'



start:
ifeq ("$(wildcard $(DEV_PY))","")
	@echo "Creating py.dev file"
	@cp CONFIG.py CONFIG.py.dev
else
	@echo "file $(DEV_PY) exist"
endif
	@mv CONFIG.py CONFIG.py.old
	@mv CONFIG.py.dev CONFIG.py

ifneq ("$(wildcard $(VIERTUAL_ENV))","")
	@source "$(VIRTUAL_ENV)"
else
	@echo "No virtualenv found"
endif

clean:
ifeq ("$(wildcard $(OLD_PY))","")
	@echo "File py.old not found"
else
	@echo "File py.old found"
	@mv CONFIG.py CONFIG.py.dev
	@mv CONFIG.py.old CONFIG.py
endif
ifeq ("$(wildcard $(SESSION))","")
	@echo "No pastebot.session file"
else
	@echo "Deleting session file"
	@rm partebot.session
endif
