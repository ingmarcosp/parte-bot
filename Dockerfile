FROM python:3
MAINTAINER Marcos P. <contacto@marcosp.it>

COPY requirements.txt /tmp
RUN apt update && apt install libfbclient2 -y
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt

VOLUME ["/partebot"]

WORKDIR /partebot
ENTRYPOINT [ "python3", "/partebot/partebot.py" ]